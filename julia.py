#!/usr/bin/env python
"""
julia.py: Generate images of Julia and Mandelbrot sets
Version 0.1 (November 25, 2018)

Copyright (C) 2018 Alexander Gosselin <alex@oddloop.ca>

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>
"""
from __future__ import print_function  # Python 2 compatibility

import sys  # provides sys.exit()
import array
import signal  # see <https://stackoverflow.com/a/44869451/2738025>
import argparse  # convenient command-line argument parsing
import PIL.Image  # image data type
import PIL.ImageColor  # contains choices for colorize function
import PIL.ImageOps  # image transformation operations

from cmath import *  # math functions for definition of f
from itertools import product  # used to iterate over pixels
from contextlib import closing  # used for Python 2 compatibility
from multiprocessing import Pool, cpu_count  # multiprocessing for speed


class IteratedSetImage:
    """Abstract base class for JuliaSetImage and MandelbrotSetImage classes."""

    def __init__(
        self, function, size, center=0.0 + 0.0j, levels=256, gamma=1.0, zoom=1.0
    ):
        self.size = size
        self.zoom = zoom
        self.scale = 4.0 / (self.zoom * max(self.size))
        self.center = center
        self.title = function
        self.set_f(function)
        self.levels = levels
        self.gamma = gamma
        self.lookup_table = array.array(
            "B",
            (
                int(255.0 * pow(i / (self.levels - 1.0), self.gamma))
                for i in range(self.levels)
            ),
        )

        def init_worker():
            signal.signal(signal.SIGINT, signal.SIG_IGN)

        with closing(Pool(cpu_count(), init_worker)) as pool:
            try:
                data = pool.map(
                    self, product(range(self.size[1]), range(self.size[0]))
                )
            except KeyboardInterrupt:
                pool.terminate()
                pool.join()
                raise KeyboardInterrupt
        self.image = PIL.Image.frombytes("L", self.size, bytes(bytearray(data)))

    def __call__(self, pixel):
        raise NotImplementedError

    def set_f(self, function):
        exec("def _f(z): return " + function, globals())
        self.f = _f

    def colorize(self, black, white):
        return PIL.ImageOps.colorize(self.image, black, white)

    def save(self, filename):
        self.image.save(filename)

    def show(self):
        self.image.show()


class JuliaSetImage(IteratedSetImage):
    def __call__(self, pixel):
        z = complex(
            self.scale * (pixel[1] - self.size[0] / 2.0) + self.center.real,
            self.scale * (self.size[1] / 2.0 - pixel[0]) + self.center.imag,
        )
        for i in range(self.levels):
            try:
                z = self.f(z)
            except ZeroDivisionError:
                break
            if abs(z) > 2.0:
                break
        return self.lookup_table[i]


class MandelbrotSetImage(IteratedSetImage):
    def __call__(self, pixel):
        c = complex(
            self.scale * (pixel[1] - self.size[0] / 2.0) + self.center.real,
            self.scale * (self.size[1] / 2.0 - pixel[0]) + self.center.imag,
        )
        z = 0.0 + 0.0j
        for i in range(self.levels):
            try:
                z = self.f(z) + c
            except ZeroDivisionError:
                break
            if abs(z) > 2:
                break
        return self.lookup_table[i]


def main(
    function,
    size,
    quiet=False,
    colors=None,
    save=None,
    Mandelbrot=False,
    **kwargs
):
    try:
        image = (
            JuliaSetImage(function, size, **kwargs)
            if Mandelbrot
            else MandelbrotSetImage(function, size, **kwargs)
        )
    except KeyboardInterrupt:
        sys.exit()
    if colors is not None:
        image = image.colorize(colors[0], colors[1])
    if save is not None:
        image.save(save)
    if quiet is False:
        image.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "function", type=str, help="f(z) formatted as a Python expression"
    )
    parser.add_argument("width", type=int, help="width of image in pixels")
    parser.add_argument("height", type=int, help="height of image in pixels")
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="do not show image"
    )
    parser.add_argument(
        "-s",
        "--save",
        type=str,
        metavar="FILENAME",
        help="location at which to save image",
    )
    parser.add_argument(
        "-l",
        "--levels",
        type=int,
        metavar="[1-256]",
        help="number of color levels in image",
        default=256,
    )
    parser.add_argument(
        "-c",
        "--colors",
        nargs=2,
        type=str,
        metavar=("BLACK", "WHITE"),
        choices=list(PIL.ImageColor.colormap.keys()),
        help="colors to replace back and white in image",
    )
    parser.add_argument(
        "-g", "--gamma", type=float, help="gamma correction to apply to image"
    )
    parser.add_argument(
        "-z", "--zoom", type=float, help="zoom factor for the image"
    )
    parser.add_argument(
        "-C",
        "--center",
        type=str,
        help="the center of the image in the complex plane as a string",
    )
    parser.add_argument(
        "-M",
        "--Mandelbrot",
        action="store_false",
        help="plot a Mandelbrot set instead of a Julia set",
    )
    args = parser.parse_args()
    kwargs = {
        key: value
        for key, value in vars(args).items()
        if key not in ("function", "width", "height") and value is not None
    }
    if "center" in kwargs:
        kwargs["center"] = complex(kwargs["center"].replace(" ", ""))
    size = (args.width, args.height)
    main(args.function, size, **kwargs)
